<?php
/**
 * created by Duran Can Yılmaz
 */
class paytr_dcy
{
  private $merchant_id,$merchant_key,$merchant_salt,$email,$payment_amount,$merchant_oid,$user_name,$user_address,$user_phone,
  $merchant_ok_url,$merchant_fail_url,$user_basket,$user_basket_raw,$user_ip,$domain,$timeout_limit,$debug_on,$test_mode,$no_installment,$max_installment,$currency,$token;

  function __construct(int $merchant_id,string $merchant_key,string $merchant_salt)
  {
     self::set_ip();
     self::set_options();
     $this->domain=$_SERVER['SERVER_NAME'];
     $this->merchant_id=$merchant_id;
     $this->merchant_key=$merchant_key;
     $this->merchant_salt=$merchant_salt;
  }
  function set_ip(){
    if( isset( $_SERVER["HTTP_CLIENT_IP"] ) ) {
  		$ip = $_SERVER["HTTP_CLIENT_IP"];
  	} elseif( isset( $_SERVER["HTTP_X_FORWARDED_FOR"] ) ) {
  		$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
  	} else {
  		$ip = $_SERVER["REMOTE_ADDR"];
  	}
    $this->user_ip=$ip;
  }
  function get_ip(){
    return $this->user_ip;
  }
  function get_basket(){
    return $this->user_basket_raw;
  }
  function get_amount(){
    return $this->payment_amount;
  }
  function set_options(string $merchant_ok_url="success", string $merchant_fail_url="error",string $timeout_limit = "30",int $debug_on = 1,int $test_mode = 1,int $no_installment	= 0,int $max_installment = 0,string $currency = "TL"){
    $this->merchant_ok_url=$this->domain.$merchant_ok_url;
    $this->merchant_fail_url=$this->domain.$merchant_fail_url;
    $this->timeout_limit=$timeout_limit;
    $this->debug_on=$debug_on;
    $this->test_mode=$test_mode;
    $this->no_installment=$no_installment;
    $this->max_installment=$max_installment;
    $this->currency=$currency;
  }
  function set_user($user_name,$user_address,$user_phone,$email,array $user_basket){
    $this->user_name=$user_name;
    $this->user_address=$user_address;
    $this->user_phone=$user_phone;
    $this->email=$email;
    $this->payment_amount=0;
    foreach ($user_basket as $key => $v) {
      $adet=$v[2];
      $fiyat=$v[1];
      $this->payment_amount+=(double)$adet*(double)$fiyat*100;
    }
    $this->user_basket_raw=$user_basket;
    $this->user_basket=base64_encode(json_encode($user_basket));
  }
  function order_no(int $spno){
    $this->merchant_oid=$spno;
  }
  function get_token(){
    $hash_str = $this->merchant_id .$this->user_ip .$this->merchant_oid .$this->email .$this->payment_amount .$this->user_basket.$this->no_installment.$this->max_installment.$this->currency.$this->test_mode;
  	$paytr_token=base64_encode(hash_hmac('sha256',$hash_str.$this->merchant_salt,$this->merchant_key,true));
  	$post_vals=array(
  			'merchant_id'=>$this->merchant_id,
  			'user_ip'=>$this->user_ip,
  			'merchant_oid'=>$this->merchant_oid,
  			'email'=>$this->email,
  			'payment_amount'=>$this->payment_amount,
  			'paytr_token'=>$paytr_token,
  			'user_basket'=>$this->user_basket,
  			'debug_on'=>$this->debug_on,
  			'no_installment'=>$this->no_installment,
  			'max_installment'=>$this->max_installment,
  			'user_name'=>$this->user_name,
  			'user_address'=>$this->user_address,
  			'user_phone'=>$this->user_phone,
  			'merchant_ok_url'=>$this->merchant_ok_url,
  			'merchant_fail_url'=>$this->merchant_fail_url,
  			'timeout_limit'=>$this->timeout_limit,
  			'currency'=>$this->currency,
              'test_mode'=>$this->test_mode
  		);

    	$ch=curl_init();
    	curl_setopt($ch, CURLOPT_URL, "https://www.paytr.com/odeme/api/get-token");
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_POST, 1) ;
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vals);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    	curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
    	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    	$result = @curl_exec($ch);

    	if(curl_errno($ch)){die("PAYTR IFRAME connection error. err:".curl_error($ch));}
    	curl_close($ch);
    	$result=json_decode($result,1);
    	if($result['status']=='success')
    		{
          $token=$result['token'];
          return $token;
        }else{die("PAYTR IFRAME failed. reason:".$result['reason']);}
    }
    function get_info(){
      @ob_start();
      var_dump($this->user_basket_raw);
      $basket_raw=@ob_get_clean();
      echo"
      <br /><br /><h4>###settings</h4>
      <br /><b>merchant_ok_url</b> $this->merchant_ok_url
      <br /><b>merchant_fail_url</b> $this->merchant_fail_url
      <br /><b>timeout_limit</b> $this->timeout_limit
      <br /><b>test_mode</b> $this->test_mode
      <br /><b>debug_on</b> $this->debug_on
      <br /><b>no_installment</b> $this->no_installment
      <br /><b>max_installment</b> $this->max_installment
      <br /><b>currency</b> $this->currency
      <br /><br /><h4>###user</h4>
      <br /><b>user_name</b> $this->user_name
      <br /><b>user_address</b> $this->user_address
      <br /><b>user_phone</b> $this->user_phone
      <br /><b>email</b> $this->email
      <br /><b>payment_amount</b> $this->payment_amount
      <br /><b>user_basket_raw</b> $basket_raw
      <br ><br />###others
      <br >'merchant_id'=>$this->merchant_id,
      <br >'user_ip'=>$this->user_ip,
      <br >'merchant_oid'=>$this->merchant_oid,
      <br >'email'=>$this->email,
      <br >'payment_amount'=>$this->payment_amount,

      <br >'user_basket'=>$this->user_basket,
      <br >'debug_on'=>$this->debug_on,
      <br >'no_installment'=>$this->no_installment,
      <br >'max_installment'=>$this->max_installment,
      <br >'user_name'=>$this->user_name,
      <br >'user_address'=>$this->user_address,
      <br >'user_phone'=>$this->user_phone,
      <br >'merchant_ok_url'=>$this->merchant_ok_url,
      <br >'merchant_fail_url'=>$this->merchant_fail_url,
      <br >'timeout_limit'=>$this->timeout_limit,
      <br >'currency'=>$this->currency,
      <br >'test_mode'=>$this->test_mode
      ";
    }
  function show(string $width="100%;"){
    $this->token=self::get_token();
    echo'<!-- Ödeme formunun açılması için gereken HTML kodlar / Başlangıç -->'.
    '<script src="https://www.paytr.com/js/iframeResizer.min.js"></script>'.
    '<div id="paytr"><iframe src="https://www.paytr.com/odeme/guvenli/'.$this->token.'" id="paytriframe" frameborder="0" scrolling="no" style="width:'.$width.'"></iframe></div>'.
    "<script>iFrameResize({},'#paytriframe');</script>".
    '<!-- Ödeme formunun açılması için gereken HTML kodlar / Bitiş -->';
  }
}

// paytr_dcy(MAĞZA_KODU,MAĞZA_KEY,MAĞZA_SALT); // bu kodları paytr sitesinden alabilirsiniz.
$paytr=new paytr_dcy(MAĞZA_KODU,MAĞZA_KEY,MAĞZA_SALT);
//sepet array şeklinde ve alttaki dizilime göre olmalıdır. Ürün fiyatı neyse onu girin zaten fiyat hesaplanırken 100 ile çarpılacak.
$basket=array(
  array("Örnek ürün 1", "10.00", 1), // 1. ürün (Ürün Ad - Birim Fiyat - Adet )
  array("Örnek ürün 2", "24.50", 2), // 2. ürün (Ürün Ad - Birim Fiyat - Adet )
  array("Örnek ürün 3", "11.00", 1)  // 3
);
//müşteriyi bu şekilde ekleyebilirsiniz. Müşteriyi ekledikten sonra sepet(basket)ini eklemeyi unutmayın
$paytr->set_user("Duran Can Yılmaz","adres","telefon","durancanyilmaz@hotmail.com",$basket);
$paytr->order_no(rand(0,999999));//sipariş numarası uniq olmalı kendi sipariş numaranızı verirsiniz ben şimdilik random yaptım.
$paytr->set_options("?success=1","?fail=1");
$paytr->show();//en son çağırılması gerekiyor.
// $paytr->get_info(); debug yapmak isterseniz dataları görebilirsiniz


?>
